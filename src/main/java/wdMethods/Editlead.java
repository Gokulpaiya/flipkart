package wdMethods;

import org.apache.poi.openxml4j.opc.PackageNamespaces;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Editlead extends ProjectSpecificMethods {
@Test(groups = "sanity")


		public void login() throws InterruptedException{		
	WebElement crmlink =locateElement("link","CRM/SFA");
			click(crmlink);
			WebElement editlead = locateElement ("link","Leads");
			click (editlead);
			WebElement findleads = locateElement ("link","Find Leads");
			click (findleads);
			WebElement firstname = locateElement ("xpath","//input[@name='id']/following::input");
			type (firstname, "Gokul");
			WebElement findleadsbutton = locateElement ("xpath","//button[text()='Find Leads']");
			click (findleadsbutton);
			Thread.sleep(3000);
			WebElement selectlead = locateElement ("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a[1]");
			click (selectlead);	
			WebElement editlead1 = locateElement ("xpath","//a[text()='Edit']");
			click (editlead1);
			WebElement companyname =locateElement ("id","updateLeadForm_companyName");
			type (companyname,"thryve");
			WebElement submit = locateElement ("classname", "smallSubmit");
			click (submit);
			//WebElement verify =locateElement("id","viewLead_companyName_sp");
			String text2 = driver.findElementById("viewLead_companyName_sp").getText();
			if (text2.contains("thryve"))
				System.out.println("Companyname is verified");
			else System.out.println("Companyname is not verified");
			
	}

}
