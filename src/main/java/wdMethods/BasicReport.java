package wdMethods;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
@Test
public class BasicReport {
	public void startResult() throws IOException {
		 ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		 html.setAppendExisting(true);
		 ExtentReports extent = new ExtentReports(); 
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("login", "login to leaftaps");
		test.pass("user name is entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		//test.fail("user name is not entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img10.png").build());
		test.assignAuthor("Gokul");
		test.assignCategory("smoke");
		extent.flush();
	}

}
