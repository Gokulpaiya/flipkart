package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectSpecificMethods extends SeMethods  {
	@BeforeMethod
	public void doLogin(){
	startApp("chrome","http://leaftaps.com/opentaps");
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, "crmsfa");
	WebElement elelogin = locateElement("classname","decorativeSubmit");
	click(elelogin);
	}
	//@Parameters({"browser","url","uname","pwd"})
	//@BeforeMethod
	//public void doLogin(String browserName,String URL, String username,String password)
	//{
		//startApp(browserName,URL);
		//WebElement eleUserName = locateElement("id", "username");
		//type(eleUserName, username);
		//WebElement elePassword = locateElement("id", "password");
		//type(elePassword, password);

		
	@AfterMethod
	public void doClose() {
	driver.close();
	}
	
}
