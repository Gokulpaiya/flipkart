package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class createlead extends ProjectSpecificMethods {

	@Test (groups = "smoke")
public void logincreate(){
WebElement crmlink =locateElement("link","CRM/SFA");
			click(crmlink);
	WebElement createlead=locateElement("link","Create Lead");
	click(createlead);
	WebElement companyname=locateElement("id","createLeadForm_companyName");
	type (companyname, "thryve");
	WebElement firstname=locateElement("id","createLeadForm_firstName");
	type (firstname, "Gokul");
	WebElement lastname=locateElement("id","createLeadForm_lastName");
	type (lastname,"Raj");
	//WebElement Parentaccount=locateElement("id", "createLeadForm_parentPartyId");
	//type(Parentaccount,"34567");
	WebElement source=locateElement ("id" ,"createLeadForm_dataSourceId");
	selectDropDownUsingText(source, "Partner");
	WebElement marketingcompaign=locateElement("id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingText(marketingcompaign, "Automobile");
	WebElement firstnamelocal=locateElement("id", "createLeadForm_firstNameLocal");
	type (firstnamelocal,"Gok");
	WebElement lastnamelocal=locateElement ("id","createLeadForm_lastNameLocal");
	type (lastnamelocal,"P");
	WebElement salutation=locateElement ("id","createLeadForm_personalTitle");
	type (salutation, "Ms");
	WebElement title=locateElement ("id", "createLeadForm_generalProfTitle");
	type (title,"sir");
	WebElement department=locateElement("id","createLeadForm_departmentName");
	type (department,"Testing");
	WebElement annualrevenue=locateElement("id","createLeadForm_annualRevenue");
	type (annualrevenue,"800000");
	WebElement noemployees=locateElement("id","createLeadForm_numberEmployees");
	type (noemployees,"5000");
	WebElement industry=locateElement("id", "createLeadForm_industryEnumId");
	selectDropDownUsingIndex(industry,3);
	WebElement ownership=locateElement("id", "createLeadForm_ownershipEnumId");
	selectDropDownUsingText(ownership, "Corporation");
	WebElement symbol=locateElement("id","createLeadForm_tickerSymbol");
	type (symbol,"Tiger Symbol");
	WebElement submit=locateElement("classname","smallSubmit");
	click (submit);
	String text2 = driver.findElementById("viewLead_firstName_sp").getText();
	if (text2.contains("Gokul"))
		System.out.println("Firstname is verified");
	else System.out.println("Firstname is not verified");
	}

}
