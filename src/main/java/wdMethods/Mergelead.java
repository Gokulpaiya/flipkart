package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.SeMethods;
public class Mergelead extends SeMethods {
@Test(dependsOnGroups = "smoke")
		public void login() throws InterruptedException{
			startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			type(elePassword, "crmsfa");
			WebElement elelogin = locateElement("classname","decorativeSubmit");
			click(elelogin);
			WebElement crmlink =locateElement("link","CRM/SFA");
			click(crmlink);
			WebElement leads = locateElement("link","Leads");
			click(leads);
			WebElement Mergeleads = locateElement("link","Merge Leads");
			click(Mergeleads);
			WebElement fromleadicon = locateElement("classname","Lookup");
			click(fromleadicon);
}}