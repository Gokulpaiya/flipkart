package wdMethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Login extends SeMethods{
	@Test
	public void login(){
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
		WebElement elelink = locateElement("link","CRM/SFA");
		click(elelink);
		WebElement elecreatelead = locateElement("link","Create Lead");
		click(elecreatelead);
		WebElement elecompname = locateElement("id", "createLeadForm_companyName");
	    type(elecompname, "Thryve");
	    WebElement enterfirstname = locateElement("id", "createLeadForm_firstName");
	    type(enterfirstname,"Gokul");
	    WebElement enterlastname = locateElement("id","createLeadForm_lastName");
        type(enterlastname,"Raj");
        WebElement enterfstnmelocal = locateElement("id", "createLeadForm_firstNameLocal");
	    type(enterfstnmelocal,"Gokul");
	    WebElement enterlstnme = locateElement("id","createLeadForm_lastNameLocal");
	    type(enterlstnme,"Raj");
	    WebElement entersalutation = locateElement("id", "createLeadForm_personalTitle");
	    type(entersalutation,"TestEngineer");
	    WebElement dropdownsource = locateElement("id", "createLeadForm_dataSourceId");
	    selectDropDownUsingText(dropdownsource, "Public Relations");
	    verifySelected(dropdownsource);
	    WebElement industrydropdown = locateElement("id","createLeadForm_industryEnumId");
	    selectDropDownUsingIndex(industrydropdown,2);
	    verifyDisplayed(industrydropdown);
	    WebElement getext = locateElement("classname", "tableheadtext");
	    getText(getext);
	}
	
}









