package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.SeMethods;
public class Deletelead extends SeMethods {
@Test(groups = "regression")
		public void login() throws InterruptedException{
			startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			type(elePassword, "crmsfa");
			WebElement elelogin = locateElement("classname","decorativeSubmit");
			click(elelogin);
			WebElement crmlink =locateElement("link","CRM/SFA");
			click(crmlink);
			WebElement editlead = locateElement ("link","Leads");
			click (editlead);
			WebElement findleads = locateElement ("link","Find Leads");
			click (findleads);
			WebElement firstname = locateElement ("xpath","//input[@name='id']/following::input");
			type (firstname, "Gokul");
			WebElement findleadsbutton = locateElement ("xpath","//button[text()='Find Leads']");
			click (findleadsbutton);
			Thread.sleep(3000);
			String selectlead = locateElement ("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a[1]").getText();
			WebElement selectlead1 = locateElement ("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a[1]");
			click (selectlead1);	
			WebElement deletelead1 = locateElement ("xpath","//a[text()='Delete']");
			click (deletelead1);
			WebElement checklead = locateElement ("xpath","//a[text()='Find Leads']");
			click (checklead);
			WebElement leadid = locateElement ("xpath","//input[@name='id']");
			type (leadid,selectlead);
			WebElement findleadsbutton1 = locateElement ("xpath","//button[text()='Find Leads']");
			click (findleadsbutton1);
			String noleads = locateElement ("xpath","//div[text()='No records to display']").getText();
			if (noleads.contains("No records"))
				System.out.println("Lead is deleted");
			else System.out.println("Lead is available");
	}

}
