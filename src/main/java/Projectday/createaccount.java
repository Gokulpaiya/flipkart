package Projectday;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;

public class createaccount extends ProjectSpecificMethods{
	@Test
	public void login(){
		WebElement crmlink =locateElement("link","CRM/SFA");
		click(crmlink);
		WebElement Accounts =locateElement("link","Accounts");
					click(Accounts);
		WebElement createAccount =locateElement("link","Create Account");
					click(createAccount);
		WebElement accountname=locateElement("id","accountName");
		type (accountname, "KeplersAccount");
		WebElement industry=locateElement ("name" ,"industryEnumId");
		selectDropDownUsingText(industry, "Computer Software");
		WebElement currency=locateElement ("id","currencyUomId");
		selectDropDownUsingText(currency, "INR - Indian Rupee");
		WebElement source=locateElement ("id","dataSourceId");
		selectDropDownUsingIndex(source, 2);
		WebElement marketingcampaign=locateElement("id","marketingCampaignId");
		selectDropDownUsingText(marketingcampaign,"Catalog Generating Marketing Campaigns");
        WebElement phone=locateElement("id","primaryPhoneNumber");
        type(phone, "8056854189");
        WebElement city=locateElement("id","generalCity");
        type(city, "chennai");
        WebElement email=locateElement("id","primaryEmail");
        type(email,"gokulpaiaya@yahoo.com");
        WebElement country=locateElement("name","generalCountryGeoId");
        selectDropDownUsingText(country, "India");
        WebElement state=locateElement("name","generalStateProvinceGeoId");
        selectDropDownUsingText(state, "TAMILNADU");
        WebElement submit=locateElement("classname","smallSubmit");
    	click (submit);
    	WebElement gettxt=locateElement("xpath","//span[@class='tabletext']/child::a[2]");
        getText(gettxt);
        WebElement findaccount=locateElement("link","Find Accounts");
        click(findaccount);
        //WebElement typeaccountname=locateElement("name","accountName");
        //type(typeaccountname,gettxt);
        
	}
}
