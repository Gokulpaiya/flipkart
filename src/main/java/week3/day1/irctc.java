package week3.day1;

import java.sql.Driver;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class irctc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\TestLeaf\\eclipse-workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable DataTableHeader TrainList']");
		List<WebElement> row = table.findElements(By.tagName("tr"));
        System.out.println(row.size());		
        //WebElement secondrow = row.get(1);
        //List<WebElement> column = secondrow.findElements(By.tagName("td"));
        //System.out.println(column.size());
		//String text = column.get(1).getText();
		//System.out.println(text);
		driver.close();
	}

}
