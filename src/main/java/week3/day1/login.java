package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import net.bytebuddy.implementation.bytecode.constant.SerializedConstant;

public class login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\TestLeaf\\eclipse-workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
        driver.findElementByClassName("decorativeSubmit").click();
        driver.findElementByLinkText("CRM/SFA").click();
        driver.findElementByLinkText("Create Lead").click();
        driver.findElementById("createLeadForm_companyName").sendKeys("testleaf");
        driver.findElementById("createLeadForm_firstName").sendKeys("Gokul");
        driver.findElementById("createLeadForm_lastName").sendKeys("Raj");
        WebElement source = driver.findElementById("createLeadForm_dataSourceId");
        Select sc=new Select(source);
        sc.selectByVisibleText("Public Relations");
        driver.findElementByXPath("//img[@alt='Lookup']").click();
        //window handling
        Set<String> allWindows = driver.getWindowHandles();
        List<String> lst  = new ArrayList<>();
        lst.addAll(allWindows);
        driver.switchTo().window(lst.get(1));
        driver.findElementByPartialLinkText("10047").click();
        driver.switchTo().window(lst.get(0));
        //WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
        //Select mk=new Select(marketing);
        //mk.selectByValue("CATRQ_CARNDRIVER");
        //WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
        //Select in=new Select(industry);
        //List<WebElement> options = in.getOptions();
        //int count = options.size();
        //in.selectByIndex(count-3);
        //driver.findElementByName("submitButton").click();
        //driver.close();
	}

}
