package Reports;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	public String testcaseName, testDesc, author, category;
	
	@BeforeSuite
	public void startResult() {
		html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);  
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	@BeforeMethod
	public void startTestcase(){
		test = extent.createTest(testcaseName, testDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void logStep(String desc, String status) {
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("FAIL")) {
			test.fail(desc);
		}else if(status.equalsIgnoreCase("WARN")) {
			test.warning(desc);
		}
	}
	
	
	@AfterSuite
	public void endResult() {
		extent.flush(); 
	}
}