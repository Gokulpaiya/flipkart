package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class learnexcel {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("C:\\TestLeaf\\eclipse-workspace\\Selenium\\data\\createlead.xlsx");
		XSSFSheet sheet = wbook.getSheet("createlead");
		int rowCount = sheet.getLastRowNum();
		System.out.println("row count is: "+rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("columnCount: "+columnCount);
		for (int i=1; i<=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j=0; j<columnCount; j++) {
				XSSFCell cell = row.getCell(j);
				String stringcellvalue = cell.getStringCellValue();
				System.out.println(stringcellvalue);
			}
		}

	}

}
